---
title: "Flatpak - an insecurity nightmare"
description: "For over 20 years, Linux has been used everywhere, from servers to embedded devices. One area it has been lacking in, however, is the desktop space.

Over the last decade, the Linux desktop has been improving dramatically: Flatpak, Wayland, Portals, PipeWire, and FreeDesktop have all been working to make Linux an operating system better suited to modern (desktop) systems. 

I'm not going to go over all of those though; as you could probably tell by the title, this is going to be about *Flatpak!*"
layout: post
toc: true
tags: ["Flatpak", "Linux", "Containers"]
date: 2023-06-15
---
*Not to be confused with [Flatpack](https://en.wikipedia.org/wiki/Flatpack "Flatpack").*

For over 20 years, Linux has been used everywhere, from servers to embedded devices. One area it has been lacking in, however, is the desktop space. Windows and macOS have constantly been keeping Linux out of the market, with gaming primarily being a Windows thing and content creators utilizing macOS. 

Over the last decade, the Linux desktop has been improving dramatically: Flatpak, Wayland, Portals, PipeWire, and FreeDesktop have all been working to make Linux an operating system better suited to modern (desktop) systems. I'm not going to go over all of those though; as you could probably tell by the title, this is going to be about *Flatpak!*

## History

Let's take a quick look at the server space. It's not uncommon to see them running decade-old (sometimes even *decades*-old!) kernels and libraries. This makes sense, as their boot times are long and on every single system update, there's a risk of breakage, and needing to debug that; valuable uptime lost, especially for a service as large as Google. Naturally, they evolved a way to quickly patch the most relevant parts of the system, so security isn't too much of an issue.

However, the software they run may only be able to utilize newer features from newer libraries, not the old and crusty software the servers run. So how did they solve that?

Containers. Normally used to ship software to several customers in a consistent and safer way, they allow you to isolate software into a little box with all of the needed dependencies, at their specified versions, with limited access to the host system. 

## The present

We have Android, macOS, and iOS, all with limited access to the system, requiring end-user intervention to access certain resources. But what about the Linux desktop?

In 2015, Flatpak was released, taking all of the technology used from containerization on servers, and applying it to desktop systems. With it came [Portals](https://theevilskeleton.gitlab.io/2023/05/11/overview-of-flatpaks-permission-models.html#xdg-desktop-portals), and the [Freedesktop SDK](https://freedesktop-sdk.io/). This gave a consistent and generic platform and API to developers, a drastic change from what was previously a battleground of varying dependency versions, core system libraries, and packaging formats. 

Of course, after having the distribution and development situation stay the same for a few decades, this was too large of a change to make at once. Many of the needed APIs exist, but developers need to take time out of their day to migrate to them, and some users (specifically, Flatpak-hating ones) vehemently refuse to help improve the situation, insisting we stick with the old way of doing things, and repackage software hundreds of times for thousands of varying systems.

To put it simply, naturally, nobody in their right mind gave a shit. Many new application developers are creating their apps with a confined environment in mind, and shipping their software to Flathub, allowing the software to get to end users in a fast and secure way. 

### Application confinement

What about the old applications, though? You can find 10-year-old apps shipped on Flathub, after all. From these applications spawned articles such as [Flatpak - a security nightmare](https://flatkill.org) *hey, you might recognize that name!*, and various [responses from well-known individuals](https://theevilskeleton.gitlab.io/2021/02/11/response-to-flatkill-org.html) within the FOSS space intended to counteract misinformation and aggression from those articles.

For these apps, the FUD articles like to cherry-pick the worst offenders and use this to say that Flatpak isn't worth pushing for at all. So I'd like to address that.

These applications were designed with an unconfined environment, and the APIs necessary for them either didn't exist at the time or they weren't updated to use the new API. Cherry-picking them to show that the Flatpak sandbox is ["not a sandbox"](https://hanako.codeberg.page/) is not a valid argument, and only shows that the applications themselves are insecure. 

Of course, older apps aren't the only offenders. As the new APIs might not be mature enough, both newer and older apps may have to resort to [static permissions](https://theevilskeleton.gitlab.io/2023/05/11/overview-of-flatpaks-permission-models.html#static-permissions) in order to work properly. For most newer applications, this is fine, as they can restrict their permissions a *lot* more than older applications would be able to. 

Flatpak *is* a sandbox. And [Flatpak *is* secure. The *applications* are not](https://theevilskeleton.gitlab.io/2023/05/11/overview-of-flatpaks-permission-models.html#insecure). It doesn't help anyone to change the meaning of the word "sandbox". Just because the sandbox that some apps use isn't on par with iOS or Android, it doesn't disqualify it as a sandbox. If you'd like to blame Flatpak for that, *don't* be my guest. The fact that the apps rely on static permissions shows that the applications are insecure and rely on inconsistent interfaces, and in some cases, that the [current APIs have yet to improve enough for their use cases](https://theevilskeleton.gitlab.io/2023/05/11/overview-of-flatpaks-permission-models.html#why-do-static-permissions-still-exist). We should be focusing on improving that situation, not trying to tear it down.

If you want a real-world example of how a partial sandbox is still effective: [the "fractureiser" malware](https://prismlauncher.org/news/cf-compromised-alert/). Even if the malware was nonfunctional on Linux in the first place, it's still an example of how traditional packaging fails to address the fact that modern desktop systems have plenty of ways for a system to be compromised. Running software "that you trust" isn't enough to protect yourself. To quote the relevant notes from the linked post:

> If you use Linux, use the [PrismLauncher Flatpak](https://flathub.org/apps/org.prismlauncher.PrismLauncher) and be very careful of what you give it access to through utilities such as Flatseal. By default, it has no access to the needed directories to compromise the system.

> Taking a quick look over the decompiled source code, it will indeed fail to function inside of the default PrismLauncher Flatpak sandbox; the current malware hardcodes the user's ~/.config/ directory. The creation of files inside of the Flatpak sandbox, if the app does not have access to that real path, will result in it being written to a tmpfs that gets wiped on a sandbox restart. systemd is also not available inside of the Flatpak sandbox, so executing that command will fail. The malware seems to not attempt to work around these limitations, and assumes it is running unsandboxed.

*Disclaimer: I wrote the following notes :)*

*For a more in-depth view of static permissions and Portals, see [Overview of Flatpak’s Permission Models](https://theevilskeleton.gitlab.io/2023/05/11/overview-of-flatpaks-permission-models.html#why-do-static-permissions-still-exist) by [TheEvilSkeleton](https://theevilskeleton.gitlab.io/about).*

### Outdated dependencies

Some also like to bring up outdated dependencies to the argument. No, this doesn't matter. The runtime itself is updated whenever a needed security update is available (sometimes even [breaking the ABI](https://gitlab.com/freedesktop-sdk/freedesktop-sdk/-/blob/a90f92388b5c1c873168920c570769bee8a2beb1/utils/abidiff-suppressions.ini) if it's a big enough deal) and application developers don't have to think about manually updating bundled dependencies with [automatic update checking](https://github.com/flathub/flatpak-external-data-checker). And many major libraries, such as your GPU drivers, can be updated separately from your runtime if you so choose. Outdated dependencies isn't even an argument in the first place.

In the event an application *does* use outdated libraries, be it because an update broke the build process or the developer forgot to set up the data checker, there is the sandbox placed around all Flatpak applications that can help mitigate these issues while the developer addresses them. 

## The future

The hope is for the Linux desktop to migrate to developer- and more sandbox-friendly methods of software distribution and programming. We've already made great strides with PipeWire, which paves the way for *many* nice features with audio and video (including several apps using your camera at the same time!), and FreeDesktop, but we undoubtedly have a ways to go before Linux is a viable platform for developers to target and ship software to. 

We can't move forward without modern software like Wayland and Flatpak, no matter how much one might want to repair the situation with Xorg and traditional packaging. If we keep trying to revert to the old way of doing things, all that's shown to developers is that Linux isn't a platform worth considering, and it ends up being a worse situation for end users; turning back into a "which came first: the chicken or the egg?" situation, and resulting in a *very* bad UX.

We don't want that happening. 

## More reading

If you want more information on Flatpak and in general more information on how it works and the current situation, the following articles are worth a read:

- [Response to flatkill.org - TheEvilSkeleton](https://theevilskeleton.gitlab.io/2021/02/11/response-to-flatkill-org.html)

- [Response to “Flatpak Is Not the Future” - TheEvilSkeleton](https://theevilskeleton.gitlab.io/2022/05/16/response-to-flatpak-is-not-the-future.html)

- [Traditional Packaging is not Suitable for Modern Applications - TheEvilSkeleton](https://theevilskeleton.gitlab.io/2022/08/29/traditional-packaging-is-not-suitable-for-modern-applications.html)

- [Overview of Flatpak’s Permission Models - TheEvilSkeleton](https://theevilskeleton.gitlab.io/2023/05/11/overview-of-flatpaks-permission-models.html)

- [Response to “Developers are lazy, thus Flatpak” - TheEvilSkeleton](https://theevilskeleton.gitlab.io/2023/06/04/response-to-developers-are-lazy-thus-flatpak.html)

- [Distribution packaging for Linux desktop applications is unsustainable - memoryfile](https://memoryfile.codeberg.page/posts/Distribution-packaging-for-Linux-desktop-applications-is-unsustainable/)

- [You are not actually mad at Flatpak - Jordan Petridis](https://blogs.gnome.org/alatiera/2023/06/09/you-are-not-actually-mad-at-flatpak/)

<!--[Flatpak.org](https://flatpak.org) has a list of presentations and articles about Flatpak [here](https://flatpak.org/presentations/).-->
---

*[FUD]: fear, uncertainty, and doubt
