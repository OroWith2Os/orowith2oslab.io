---
title: "I lost my mind trying to bootstrap rustc with mrustc and you will too"
description: "I'm gonna build mrustc for all you funny internet people *again*. Have fun, and use this information wisely."
layout: post
toc: true
tags: ["Linux", "Containers", "Rust", "mrustc"]
date: 2023-06-24
---

I SPENT THE LAST 10 HOURS TRYING TO BUILD RUSTC AND HOLY FUCK EVERYTHING IS SO OLD AND HACKY AND FUCKED UP THAT I DONT KNOW WHAT JUST HAPPENED BUT NOW YOURE GOING TO GO THROUGH IT WITH ME AGAIN

THATS RIGHT

IM GOING TO SUFFER THROUGH BUILDING RUSTC FROM SOURCE AGAIN, WITHOUT RUSTC ITSELF, WITHOUT THE ORIGINAL RUST COMPILER, ALL FOR THIS FUNNY INTERNET BLOG POST THAT FIVE PEOPLE WILL READ

UHUH

BUILDING RUSTC WITHOUT RUSTC AND LOSING MY SANITY AGAIN IN THE PROCESS

*for the love of god and all that is holy please let this go well*

## GETTING STARTED

TO START US OFF lets make ourselves a container

THATS RIGHT A CONTAINER

WE GONNA GET GOOFY TODAY

```sh
distrobox create -n CrystalBoxFUCK -i registry.getcryst.al/crystal/misc/docker:latest --home $HOME/distroboxes/CrystalBoxFUCK
```

Why this niche distro in particular you might ask? No, because FUCK YOU 

NOW LETS GET STARTED WITH MRUSTC and ENTER THE CONTAINER
```sh
distrobox enter CrystalBoxFUCK
git clone https://github.com/thepowersgang/mrustc.git
cd mrustc
git checkout b4503ee66847581e0483b6cff0ebc3a3d99fb4ff # because we don't want you cheating and taking advantage of any fixes until we fix them ourselves, now do we?
```

NOW lets **BUILD IT**. We'll go with 1.39, because thats what I used first and I don't want to spend ANOTHER TEN HOURS finding what needs to be `include`d 

```
./build-1.39.0.sh
```

wait fuck we need to download the base build dependencies or something and whatever the fuck else

```
ame ins cmake python --noconfirm
```

NOW if you wait a while for it all to build and fuck around youll see that mrustc likes to complain when it's not on <= gcc12

lets tell it "fucking take it, I don't want this `include`"

we're gonna add `#include <cstdint>`:

`src/mir/mir.hpp`
```c
/*
 * MRustC - Rust Compiler
 * - By John Hodge (Mutabah/thePowersGang)
 *
 * mir/mir.hpp
 * - MIR (Middle Intermediate Representation) definitions
 */
#pragma once
#include <tagged_union.hpp>
#include <vector>
#include <string>
#include <memory>   // std::unique_ptr
#include <hir/type.hpp>
#include "../hir/asm.hpp"
#include <int128.h>
#include <cstdint>
```

now lets continue building again with `./build-1.39.0.sh`

OH YOURE GETTING SMART WITH US

YOU WANT ANOTHER INCLUDE

`src/hir/type.cpp`
```c
/*
 * MRustC - Rust Compiler
 * - By John Hodge (Mutabah/thePowersGang)
 *
 * hir/type.cpp
 * - HIR Type helper code
 */
#include "type.hpp"
#include <span.hpp>
#include "expr.hpp" // Hack for cloning array types
#include <cstdint>
```

NOW. AGAIN. LETs BUILD

oh. my. fucking. god. one more, alright.

`tools/common/toml.h`
```c
/*
 * mrustc common tools
 * - by John Hodge (Mutabah)
 *
 * tools/common/toml.h
 * - A very basic (and probably incomplete) streaming TOML parser
 */
#pragma once

#include <fstream>
#include <vector>
#include <string>
#include <unordered_map>
#include <cstdint>
```

would you believe me if I told you this is the last patch needed? no? well, I don't blame you. but it is anyways, so we're lucky. now we can actually build rustc.

## building rustc

We're building core, and the process only just begins. 

We get through the first couple dozen steps, alright, alright, that's good.

wait till we get to LLVM. *Then* the fun begins.

now wait for a long fucking time until it also complains about some compile errors about unused imports.

what will we do about this?

FUCK IT LETs PATch IT AGAIN

`rustc-1.39.0-src/src/llvm-project/llvm/utils/benchmark/src/benchmark_register.h`
```c
#ifndef BENCHMARK_REGISTER_H
#define BENCHMARK_REGISTER_H

#include <vector>
#include <cstdint>

#include "check.h"
```

Now the final error is upon us. After this, it's a clear road, all sunshine and rainbows.

### openssl

nooOOOOOOOOOOOOOOOOOOOOOOOOOoooOOOOO

I;'m not even going to begin to start with openssl, so take this PKGBUILD and FUCK OFF

```sh
pkgname=openssl-1.1
_ver=1.1.1q
# use a pacman compatible version scheme
pkgver=${_ver/[a-z]/.${_ver//[0-9.]/}}
pkgrel=1
pkgdesc='The Open Source toolkit for Secure Sockets Layer and Transport Layer Security'
arch=('x86_64')
url='https://www.openssl.org'
license=('custom:BSD')
depends=('glibc')
makedepends=('perl')
optdepends=('ca-certificates' 'perl')
replaces=('openssl-perl' 'openssl-doc')
backup=('etc/ssl/openssl.cnf')
source=("https://www.openssl.org/source/openssl-${_ver}.tar.gz")
sha256sums=('SKIP')
validpgpkeys=('SKIP')
prepare() {
	cd "$srcdir/openssl-$_ver"
}
build() {
	cd "$srcdir/openssl-$_ver"
	# mark stack as non-executable: http://bugs.archlinux.org/task/12434
	./Configure --prefix=/opt/openssl-1.1 --openssldir=/etc/ssl-1.1 --libdir=/opt/openssl-1.1/lib \
		shared no-ssl3-method enable-ec_nistp_64_gcc_128 linux-x86_64 \
		"-Wa,--noexecstack ${CPPFLAGS} ${CFLAGS} ${LDFLAGS}"
	make depend
	make
}
package() {
	cd "$srcdir/openssl-$_ver"
	make DESTDIR="$pkgdir" MANSUFFIX=ssl install_sw install_ssldirs
}
```

I literally just took the [Arch openssl pkgbuild](https://gitlab.archlinux.org/archlinux/packaging/packages/openssl) and hacked it together to make it work. and it works. 

so run `makepkg -sif` and leave me alone.

Oh yeah and then run `set -x OPENSSL_DIR /opt/openssl-1.1/` (or `export OPENSSL_DIR=/opt/openssl-1.1`)to make rustc see openssl, for some reason it refuses to see it otherwise. I'm too lazy to debug that.

## The End.

You made it. Congratulations. Now you can work yourself up to the latest Rust compiler, cutting out a *lot* of the versions that came before, making it faster to get a fully working Rust env from scratch, no weird binaries needed. 

This was so annoying to work with, and now I have to do it for even *more* Rust versions. Holy *fuck*. At least 10 hours wasted, easily. But it all looked so simple, right? Nope. I pulled an all-nighter trying to get this to work :P

Hopefully, in the future, mrustc can more easily go right to the newest/newer Rust versions so you don't need to rebuild too much. In the meantime, have fun with this documentation regarding how I lost my mind and needed hours of sleep just to get rustc building.

Some things to keep in mind, from the mrustc developer:

> If you're building a rustc for use, then build 1.54 and step up from there
> I keep 1.19/1.29/1.39 as build targets because they're good test coverage
> (and someday I'll get time to add 1.69)

So progress :)

I'll be submitting some patches upstream to help with mrustc, including this one, which involves the mrustc changes listed here: https://github.com/thepowersgang/mrustc/pull/305
