---
order: 5
title: Socials
---

I am on the fediverse at [@orowith2os@tech.lgbt](https://tech.lgbt/@orowith2os), and currently have no plans to use any other Twitter-, Facebook-, or Instagram-like social platforms, including Bluesky. 

I am on Codeberg, GitLab (.com, .freedesktop.org, and .gnome.org), and GitHub as orowith2os. Most projects I interact with and attempt to implement are on GitHub, with longer-term projects on GitLab. 
