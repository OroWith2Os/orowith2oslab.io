---
order: 6
title: Contributions
---

I've either helped to bug test or submitted patches to the following projects:

- [Inochi2D](https://inochi2d.com): Inochi Creator and Inochi Session
- [ArmCord](https://ArmCord.xyz)
- [vpuppr](https://vpuppr.pro)

I also maintain the following Flatpaks (or Flatpak-related software):

- vpuppr: [pro.vpup.vpuppr on Flathub](https://flathub.org/apps/pro.vpup.vpuppr)
- Inochi Creator: [com.inochi2d.inochi-creator on Flathub](https://flathub.org/apps/com.inochi2d.inochi-creator)
- ArmCord: [xyz.armcord.ArmCord on Flathub](https://flathub.org/apps/xyz.armcord.ArmCord)

- The libdecor shared module: [shared-modules on Flathub](https://github.com/flathub/shared-modules/tree/master/libdecor)
- The SDL2 shared module: [shared-modules on Flathub](https://github.com/flathub/shared-modules/tree/master/SDL2)
