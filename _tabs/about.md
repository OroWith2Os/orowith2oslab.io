---
# the default layout is 'page'
icon: fas fa-info-circle
order: 4
title: About
---

Hi, I'm Oro. Some names you also might have seen are OroWith2Os, OroWwith2Os, and Dallas Strouse. 

I am from southeast Texas, and like to mess around with computers, be it with hardware or software, but you'll find me anywhere comments are possible and I might be interested. *And thus, "I am in your dependencies" came to be.*

I'm interested in the technology surrounding the Linux desktop, including Wayland, PipeWire, Flatpak, portals, and FreeDesktop and their related specifications and projects.

I use the GNOME Wayland session on Fedora Silverblue normally, and GNOME Wayland with NixOS on my laptop. My hardware (and software) is as follows:

### Main desktop

- CPU: Ryzen 5 1600 with a slight overclock
- RAM: 32GB DDR4 overclocked to 2800MHz
- GPU: AMD Radeon RX 5500XT 8GB model normally, Radeon RX 560 4GB as backup or in a dual GPU configuration
- Storage: WD Black SN750 500GB for my main Linux distribution and home folder, with 2-4TB of compressed zstd btrfs HDD storage
- Motherboard: MSI B450 Tomahawk

### Main software

- OS: I will usually use a mostly stock Fedora Silverblue, but I have a Nix flake set up and configured for GNOME and my embedded systems.
- Display server: Wayland all the way. If your software doesn't work with it, or you're a DE that doesn't support it, you automatically get shuffled off to the side. I'm not interested in Xorg whatsoever. 
-  Desktop environment: GNOME is my go-to, as it has a consistent experience on most DEs, is easily customized with extensions, is stable, and supports Wayland very well. However, sometimes I'll hop on over to Sway or KDE for a while for testing purposes.
- Application distribution: Flatpak if it's available, Distrobox otherwise. I will only run applications on the bare metal system if it's absolutely needed, like for a system service. 
- Browser: Firefox normally, but I have Edge installed in case I ever need it. 
- IDE: GNOME Builder and Neovim is what I use most of the time, as Builder integrates well with my tooling and supports Flatpak well, and Neovim is easily usable from the CLI. 
